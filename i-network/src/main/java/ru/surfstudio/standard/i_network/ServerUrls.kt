package ru.surfstudio.standard.i_network


/**
 * URL всех серверных запросов
 */

//todo определить проектные url и path

const val BASE_API_URL = "https://api.themoviedb.org/"

const val TEST_API_URL = "https://api.themoviedb.org/"

const val API_KEY = "6ccd72a2a8fc239b13f209408fc31c33"

object URLMovies {
    const val MOVIES_LIST = "3/discover/movie/"
    const val QUERY_MOVIES_LIST = "3/search/movie/"
}
