package ru.surfstudio.standard.f_main

import android.os.Parcelable
import ru.surfstudio.android.core.mvp.binding.rx.relation.mvp.Action
import ru.surfstudio.android.core.mvp.binding.rx.relation.mvp.State
import ru.surfstudio.android.core.mvp.binding.rx.ui.BindModel
import ru.surfstudio.android.core.mvp.loadstate.LoadStateInterface
import ru.surfstudio.android.dagger.scope.PerScreen
import ru.surfstudio.android.datalistpagecount.domain.datalist.DataList
import ru.surfstudio.android.easyadapter.pagination.PaginationState
import ru.surfstudio.standard.domain.movies.Movies
import javax.inject.Inject

/**
 * Модель главного экрана
 */
@PerScreen
class MainBindModel @Inject constructor() : BindModel {

    val loadState = State<LoadStateInterface>()
    val moviesState = State<DataList<Movies>>()
    val paginationState = State<PaginationState>()
    val showProgress = State<Unit>()
    val finishProgress = State<Unit>()
    var recyclerState: Parcelable? = null

    val reload = Action<Unit>()
    val loadMore = Action<Unit>()
    val loadQuery = Action<String>()
    val saveFavorite = Action<Movies>()
    val moviesClickAction = Action<Movies>()

}
