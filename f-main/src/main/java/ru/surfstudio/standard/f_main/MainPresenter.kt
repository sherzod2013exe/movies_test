package ru.surfstudio.standard.f_main

import android.util.Log
import io.reactivex.disposables.Disposables
import ru.surfstudio.android.core.mvp.binding.rx.ui.BaseRxPresenter
import ru.surfstudio.android.core.mvp.presenter.BasePresenter
import ru.surfstudio.android.core.mvp.presenter.BasePresenterDependency
import ru.surfstudio.android.dagger.scope.PerScreen
import ru.surfstudio.android.easyadapter.pagination.PaginationState
import ru.surfstudio.android.message.MessageController
import ru.surfstudio.standard.domain.movies.Movies
import ru.surfstudio.standard.ui.placeholder.LoadState
import ru.surfstudio.standard.ui.placeholder.loadstate.state.ErrorLoadState
import ru.surfstudio.standart.i_movies.MoviesInteractor
import javax.inject.Inject

/**
 * Презентер главного экрана
 */

private const val FIRST_PAGE = 1

@PerScreen
class MainPresenter @Inject constructor(
        basePresenterDependency: BasePresenterDependency,
        private val bm: MainBindModel,
        private val messageController: MessageController,
        private val moviesInteractor: MoviesInteractor
) : BaseRxPresenter(basePresenterDependency) {

    private var moviesListDisposable = Disposables.disposed()

    override fun onFirstLoad() {
        loadMovies(FIRST_PAGE)
        bm.loadMore bindTo ::loadMore
        bm.reload bindTo { loadMovies(FIRST_PAGE) }
        bm.loadQuery bindTo ::loadQuery
        bm.moviesClickAction bindTo ::showMessage
        bm.saveFavorite bindTo ::setFavorite
    }

    private fun loadQuery(query: String) {
        loadMovies(FIRST_PAGE, query)
    }

    private fun showMessage(move: Movies) {
        messageController.show(move.originalTitle)
    }

    fun setFavorite(movie: Movies) {
        moviesInteractor.setFavorite(movie.id, movie.isFavorite)
        bm.moviesState.value.map {
            if (it.id == movie.id) it.isFavorite = movie.isFavorite
        }

    }

    private fun loadMore() {
        loadMovies(bm.moviesState.value.nextPage)
    }

    private fun loadMovies(page: Int, query: String = "") {
        if (!bm.moviesState.hasValue) {
            bm.loadState.accept(LoadState.MAIN_LOADING)
        }
        if (query == "")
            bm.loadState.accept(LoadState.MAIN_LOADING)
        else
            bm.showProgress.accept()
        moviesListDisposable.dispose()
        moviesListDisposable = subscribeIoHandleError(
                moviesInteractor.getMoviesList(page, query),
                { movies ->
                    bm.loadState.accept(LoadState.NONE)
                    bm.showProgress.accept()
                    bm.finishProgress.accept()
                    if (bm.moviesState.hasValue) {
                        bm.moviesState.accept(bm.moviesState.value.merge(movies))
                    } else {
                        bm.moviesState.accept(movies)
                    }
                    bm.paginationState.accept(
                            PaginationState.READY.takeIf { bm.moviesState.value.canGetMore() }
                                    ?: PaginationState.COMPLETE
                    )
                },
                {
                    if (bm.moviesState.hasValue) {
                        bm.loadState.accept(LoadState.NONE)
                        bm.paginationState.accept(PaginationState.ERROR)
                    } else {
                        bm.loadState.accept(ErrorLoadState { loadMovies(FIRST_PAGE) })
                    }
                }
        )
    }
}