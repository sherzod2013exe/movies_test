package ru.surfstudio.standard.f_main

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.annotation.LayoutRes
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.rxkotlin.Observables
import kotlinx.android.synthetic.main.activity_main.*
import ru.surfstudio.android.core.mvp.binding.rx.ui.BaseRxActivityView
import ru.surfstudio.android.core.ui.FragmentContainer
import ru.surfstudio.android.datalistpagecount.domain.datalist.DataList
import ru.surfstudio.android.easyadapter.ItemList
import ru.surfstudio.android.easyadapter.pagination.PaginationState
import ru.surfstudio.android.template.f_main.R
import ru.surfstudio.standard.domain.movies.Movies
import ru.surfstudio.standard.f_main.di.MainScreenConfigurator
import ru.surfstudio.standard.ui.controllers.MoviesController
import ru.surfstudio.standard.ui.placeholder.isLoading
import ru.surfstudio.standard.ui.placeholder.loadstate.renderer.DefaultLoadStateRenderer
import ru.surfstudio.standard.ui.recylcer.adapter.PaginationableAdapter
import javax.inject.Inject
import android.os.CountDownTimer
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager


/**
 * Вью главного экрана
 */
class MainActivityView : BaseRxActivityView() {

    @Inject
    lateinit var bm: MainBindModel
    lateinit var mCountDownTimer: CountDownTimer

    private val adapter = PaginationableAdapter {
        bm.loadMore.accept()
    }
    private lateinit var renderer: DefaultLoadStateRenderer
    private val moviesController = MoviesController(
            onItemClick = {
                bm.moviesClickAction.accept(it)
            },
            onFavoriteClick = {
                bm.saveFavorite.accept(it)
            }
    )

    override fun createConfigurator() = MainScreenConfigurator(intent)

    @LayoutRes
    override fun getContentView(): Int = R.layout.activity_main

    override fun onCreate(
            savedInstanceState: Bundle?,
            persistentState: PersistableBundle?,
            viewRecreated: Boolean
    ) {
        initViews()
        initListeners()
        initRv()
        bind()
    }

    private fun initRv() {
        movies_rv.layoutManager = GridLayoutManager(this, resources.getInteger(R.integer.span_count))
        movies_rv.adapter = adapter
    }

    override fun getScreenName(): String = "MainActivityView"

    private fun initViews() {
    }

    private fun initListeners() {
        movies_srl.setColorSchemeResources(R.color.colorAccent)
        renderer = DefaultLoadStateRenderer(placeholder)
        movies_srl.setOnRefreshListener {
            bm.reload.accept()
        }
    }

    private fun bind() {
        Observables.combineLatest(
                bm.moviesState.observable,
                bm.paginationState.observable,
                ::createItemList
        ) bindTo { (itemList, pagingState) ->
            adapter.setItems(itemList, pagingState)
        }

        bm.loadState bindTo {
            renderer.render(it)
            if (!it.isLoading) {
                movies_srl.isRefreshing = false
            }
        }
        bm.showProgress bindTo ::showProgress
        bm.finishProgress bindTo ::finishProgress
        search_movie
                .textChanges()
                .subscribe {
                    bm.loadQuery.accept(it.toString())
                }
    }

    private fun showProgress() {
        var i: Int = 0
        progressBar.visibility = View.VISIBLE
        progressBar.progress = i
        mCountDownTimer = object : CountDownTimer(5000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                i++
                progressBar.progress = i * 100 / (5000 / 1000)
            }

            override fun onFinish() {
                //Do what you want
                i++
                progressBar.progress = 100
                progressBar.visibility = View.GONE
            }
        }
        mCountDownTimer.start()
    }

    private fun finishProgress() {
        mCountDownTimer.onFinish()
    }


    private fun createItemList(
            movies: DataList<Movies>, state: PaginationState
    ): Pair<ItemList, PaginationState> {
        return Pair(
                ItemList.create().addAll(movies, moviesController),
                state
        )
    }
}
