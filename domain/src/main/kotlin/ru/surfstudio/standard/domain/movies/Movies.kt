package ru.surfstudio.standard.domain.movies

data class Movies(
        val id: Int = 0,
        val popularity: Float = 0f,
        val posterPath: String = "",
        val originalTitle: String = "",
        val title: String = "",
        val voteAverage: Float = 0f,
        val overview: String = "",
        val releaseDate: String = "",
        var isFavorite: Boolean = false
)