package ru.surfstudio.standart.i_movies

import io.reactivex.Single
import ru.surfstudio.android.connection.ConnectionProvider
import ru.surfstudio.android.dagger.scope.PerApplication
import ru.surfstudio.android.datalistpagecount.domain.datalist.DataList
import ru.surfstudio.standard.domain.movies.Movies
import ru.surfstudio.standard.i_network.network.BaseNetworkInteractor
import javax.inject.Inject

@PerApplication
class MoviesInteractor @Inject constructor(
        connectionQualityProvider: ConnectionProvider,
        private val trainRepository: MoviesRepository,
        private val movieSaver: FavoriteMovieSaver
) : BaseNetworkInteractor(connectionQualityProvider) {

    private var query = ""

    fun getMoviesList(page: Int, query: String = ""): Single<DataList<Movies>> {
        return if (query == this.query) trainRepository.getMoviesList(page).map {
            DataList<Movies>(
                    it.map { movies ->
                        movies.isFavorite = movieSaver.get(movies.id)
                        movies
                    },
                    it.startPage,
                    it.numPages,
                    it.pageSize)
        } else trainRepository.getMoviesQuery(page, query)
    }

    fun setFavorite(id: Int, state: Boolean) {
        movieSaver.set(state, id)
    }
}