package ru.surfstudio.standart.i_movies

import android.content.SharedPreferences
import ru.surfstudio.android.dagger.scope.PerApplication
import ru.surfstudio.android.shared.pref.NO_BACKUP_SHARED_PREF
import ru.surfstudio.android.shared.pref.SettingsUtil
import javax.inject.Inject
import javax.inject.Named

private const val KEY_FAVORITE = "FAVORITE"

@PerApplication
class FavoriteMovieSaver @Inject constructor(
        @Named(NO_BACKUP_SHARED_PREF) private val noBackupSharedPref: SharedPreferences
) {
    fun get(id: Int) = SettingsUtil.getBoolean(noBackupSharedPref, id.toString(), false)
    fun set(value: Boolean, id: Int) = SettingsUtil.putBoolean(noBackupSharedPref, id.toString(), value)

}