package ru.surfstudio.standart.i_movies

import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import ru.surfstudio.android.dagger.scope.PerApplication
import ru.surfstudio.android.datalistpagecount.domain.datalist.DataList
import ru.surfstudio.standard.domain.movies.Movies
import ru.surfstudio.standard.i_network.network.transform
import ru.surfstudio.standard.i_network.service.BaseNetworkService
import javax.inject.Inject

@PerApplication
class MoviesRepository @Inject constructor(
        private val moviesApi: MoviesApi
) : BaseNetworkService() {

    fun getMoviesList(page: Int): Single<DataList<Movies>> =
            moviesApi.getMovies(page = page).transform()

    fun getMoviesQuery(page: Int, query: String) = moviesApi.searchMovies(query, page).transform()
}