package ru.surfstudio.standart.i_movies

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import ru.surfstudio.standard.i_network.API_KEY
import ru.surfstudio.standard.i_network.URLMovies
import ru.surfstudio.standart.i_movies.response.MoviesListObj

interface MoviesApi {

    @GET(URLMovies.MOVIES_LIST)
    fun getMovies(@Query("api_key") apiKey: String = API_KEY, @Query("page") page: Int): Single<MoviesListObj>

    @GET(URLMovies.QUERY_MOVIES_LIST)
    fun searchMovies(
            @Query("query") query: String,
            @Query("page") page: Int = 1,
            @Query("api_key") appKey: String = API_KEY
    ): Single<MoviesListObj>
}