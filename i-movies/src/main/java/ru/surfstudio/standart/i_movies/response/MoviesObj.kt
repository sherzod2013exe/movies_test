package ru.surfstudio.standart.i_movies.response

import com.google.gson.annotations.SerializedName
import ru.surfstudio.android.utilktx.ktx.text.EMPTY_STRING
import ru.surfstudio.standard.domain.movies.Movies
import ru.surfstudio.standard.i_network.network.Transformable

class MoviesObj(
        @SerializedName("id") private val id: Int? = null,
        @SerializedName("popularity") private val popularity: Float? = null,
        @SerializedName("poster_path") private val posterPath: String? = EMPTY_STRING,
        @SerializedName("original_title") private val originalTitle: String? = EMPTY_STRING,
        @SerializedName("title") private val title: String? = EMPTY_STRING,
        @SerializedName("vote_average") private val voteAverage: Float? = null,
        @SerializedName("overview") private val overview: String? = EMPTY_STRING,
        @SerializedName("release_date") private val releaseDate: String? = EMPTY_STRING
) : Transformable<Movies> {

    override fun transform() = Movies(
            id ?: 0,
            popularity ?: 0f,
            posterPath ?: EMPTY_STRING,
            originalTitle ?: EMPTY_STRING,
            title ?: EMPTY_STRING,
            voteAverage ?: 0f,
            overview ?: EMPTY_STRING,
            releaseDate ?: EMPTY_STRING
    )
}