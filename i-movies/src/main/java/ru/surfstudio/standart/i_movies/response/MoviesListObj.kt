package ru.surfstudio.standart.i_movies.response

import com.google.gson.annotations.SerializedName
import ru.surfstudio.android.datalistpagecount.domain.datalist.DataList
import ru.surfstudio.standard.domain.movies.Movies
import ru.surfstudio.standard.i_network.network.Transformable

class MoviesListObj(
        @SerializedName("page") private val page: Int? = null,
        @SerializedName("total_pages") private val totalPages: Int? = null,
        @SerializedName("results") private val results: List<MoviesObj>? = null
) : Transformable<DataList<Movies>> {
    override fun transform(): DataList<Movies> = DataList(
            results?.map { it.transform() } ?: emptyList(),
            page ?: 0,
            20,
            totalPages?.times(20) ?: 0,
            totalPages ?: 0
    )
}