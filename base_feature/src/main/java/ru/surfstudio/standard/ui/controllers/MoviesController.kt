package ru.surfstudio.standard.ui.controllers

import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_move.view.*
import ru.surfstudio.android.easyadapter.controller.BindableItemController
import ru.surfstudio.android.easyadapter.holder.BindableViewHolder
import ru.surfstudio.android.imageloader.ImageLoader
import ru.surfstudio.android.template.base_feature.R
import ru.surfstudio.standard.domain.movies.Movies
import ru.surfstudio.standard.ui.utils.DateFormat

class MoviesController(
        private val onItemClick: (Movies) -> Unit,
        private val onFavoriteClick: (Movies) -> Unit
) : BindableItemController<Movies, MoviesController.Holder>() {

    val imgUrl: String = "https://image.tmdb.org/t/p/original/"

    override fun getItemId(data: Movies) = data.id.toString()

    override fun createViewHolder(parent: ViewGroup) = Holder(parent)

    inner class Holder(parent: ViewGroup) : BindableViewHolder<Movies>(parent, R.layout.item_move) {

        override fun bind(data: Movies) {
            with(itemView) {
                title_tv.text = data.title
                date_tv.text = DateFormat.formatDate(data.releaseDate)
                details_tv.text = data.overview
                if (data.isFavorite)
                    favorite_iv.setImageResource(R.drawable.ic_favorite_black_24dp)
                else
                    favorite_iv.setImageResource(R.drawable.ic_heart)
                ImageLoader.with(context)
                        .url(imgUrl + data.posterPath)
                        .centerCrop(true)
                        .error(R.drawable.ic_broken_image_black_24dp, false)
                        .into(poster_iv)

                item_container.setOnClickListener {
                    onItemClick(data)
                }
                favorite_iv.setOnClickListener {
                    data.isFavorite = !data.isFavorite
                    onFavoriteClick(data)
                    if (data.isFavorite)
                        favorite_iv.setImageResource(R.drawable.ic_favorite_black_24dp)
                    else
                        favorite_iv.setImageResource(R.drawable.ic_heart)
                }
            }
        }
    }
}