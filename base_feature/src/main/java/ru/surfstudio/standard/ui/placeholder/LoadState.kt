package ru.surfstudio.standard.ui.placeholder

import ru.surfstudio.android.core.mvp.loadstate.LoadStateInterface
import ru.surfstudio.standard.ui.placeholder.loadstate.state.*

class NotFoundLoadState : LoadStateInterface
class NoInternetLoadState : LoadStateInterface

object LoadState {
    val NONE = NoneState()
    val MAIN_LOADING = MainLoadingState()
    val TRANSPARENT_LOADING = TransparentLoadingState()
    val ERROR = ErrorLoadState()
    val EMPTY = EmptyLoadState()
    val NOT_FOUND = NotFoundLoadState()
    val NO_INTERNET = NoInternetLoadState()
}

val LoadStateInterface.isLoading: Boolean
    get() = this is MainLoadingState || this is TransparentLoadingState