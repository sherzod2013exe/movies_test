package ru.surfstudio.standard.ui.utils

import java.text.SimpleDateFormat
import java.util.*

object DateFormat {
    private val fromFormat = SimpleDateFormat("yyyy-MM-dd", Locale("ru", "RU"))
    private val toFormat = SimpleDateFormat("dd MMM yyyy", Locale("ru", "RU"))

    fun formatDate(date: String): String {
        return toFormat.format(fromFormat.parse(date))
    }
}