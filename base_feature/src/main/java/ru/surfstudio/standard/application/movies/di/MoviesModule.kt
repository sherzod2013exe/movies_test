package ru.surfstudio.standard.application.movies.di

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import ru.surfstudio.android.dagger.scope.PerApplication
import ru.surfstudio.standart.i_movies.MoviesApi

@Module
class MoviesModule {

    @Provides
    @PerApplication
    internal fun provideAuthApi(retrofit: Retrofit): MoviesApi {
        return retrofit.create(MoviesApi::class.java)
    }
}